import React from 'react';
import './App.css';
import Chat from './components/Chat/Chat';
import EditModal from './components/EditModal/EditModal';

class App extends React.Component {
  render() {
    return <div>
      <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json' />
      <EditModal />
    </div>
  }
}

export default App;