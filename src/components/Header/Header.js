import React from 'react'
import './Header.css'

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

class Header extends React.Component {

    getUsersCount = () => {
        let data = this.props.messages;
        let users = data.map(msg => {
            return msg.userId
        });
        let unique = users.filter(onlyUnique);
        let usersLength = unique.length;
        return usersLength;
    }

    getTime = () => {
        let formatted = new Date(this.state[(this.props.messages.length) - 1].createdAt);
        return `${formatted.getHours()}:${formatted.getMinutes()}`
    }

    render() {

        return <div className="header" >
            <div className="header-title">SuperChat</div>
            <div className="header-users-count">{this.getUsersCount()} users</div>
            <div className="header-messages-count">{this.props.messages.length} msgs</div>
            {/* <div className="header-last-message-date">{this.getTime()} msgs</div> */}
        </div>
    }
}
export default Header;