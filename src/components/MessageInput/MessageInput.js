
import React from 'react'
import './MessageInput.css'

class MessageInput extends React.Component {

    state = { message: { id: '' }, value: '' };

    onChange = (e) => { this.setState({ value: e.target.value }) };

    onSubmit = () => {
        this.props.onSubmit({
            text: this.state.value,
            createdAt: Date().toString(),
            avatar: 'https://api-private.atlassian.com/users/8f525203adb5093c5954b43a5b6420c2/avatar',
            userId: this.props.user.userId,
            user: this.props.user.user,
            id: new Date().getTime().toString() + 5
        })
        this.setState({ value: '' })
    }

    render() {
        return <div className="message-input">
            <input className="message-input-text" type='text' value={this.state.value} onChange={this.onChange}></input>
            <button className="message-input-button" type='button' onClick={this.onSubmit}>Send</button>
        </div>
    }
}
export default MessageInput;