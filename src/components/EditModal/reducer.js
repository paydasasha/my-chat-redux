/* eslint-disable default-case */
/* eslint-disable import/no-anonymous-default-export */
import { SHOW_MODAL, HIDE_MODAL, SET_CURRENT_MESSAGE, DROP_CURRENT_MESSAGE } from "./actionTypes";

const initialState = {
    message: {
        id: '',
        userId: '',
        avatar: '',
        user: '',
        text: '',
        createdAt: '',
        editedAt: '',
        updatedAt: ''
    },
    isShown: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SHOW_MODAL: {
            return {
                ...state,
                isShown: true
            }
        }
        case HIDE_MODAL: {
            return {
                ...state,
                isShown: false
            }
        }
        case SET_CURRENT_MESSAGE: {
            const { message } = action.payload;
            return {
                ...state,
                message: message
            }
        }
        case DROP_CURRENT_MESSAGE: {
            return {
                ...state,
                message: {
                    id: '',
                    userId: '',
                    avatar: '',
                    user: '',
                    text: '',
                    createdAt: '',
                    editedAt: '',
                    updatedAt: ''
                }
            }
        }
        default:
            return state
    }
}