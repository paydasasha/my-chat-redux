import { SHOW_MODAL, HIDE_MODAL, SET_CURRENT_MESSAGE, DROP_CURRENT_MESSAGE } from "./actionTypes";

export const showModal = () => ({
    type: SHOW_MODAL,
    payload: {

    }
});

export const hideModal = () => ({
    type: HIDE_MODAL,
    payload: {

    }
});
export const setCurrentMessage = (message) => ({
    type: SET_CURRENT_MESSAGE,
    payload: {
        message
    }
});
export const dropCurrentMessage = () => ({
    type: DROP_CURRENT_MESSAGE,
    payload: {

    }
});
