/* eslint-disable no-useless-constructor */
import React from 'react';
import { updateMessage } from '../Chat/actions';
import { connect } from 'react-redux';
import * as actions from './actions';
import './EditModal.css';

class EditModal extends React.Component {

    state = {
        message: {
            id: '',
            userId: '',
            avatar: '',
            user: '',
            text: '',
            createdAt: '',
            editedAt: '',
            updatedAt: ''
        },
        value: ''
    };

    onCancel = () => {
        this.props.dropCurrentMessage();
        this.props.hideModal();
    }

    onSave = () => {
        this.props.updateMessage({
            ...this.state.message,
            text: this.state.value,
            updatedAt: Date().toString()
        });
        this.props.dropCurrentMessage();
        this.props.hideModal();
    }

    onChange = (e) => {
        this.setState({ value: e.target.value });
    };

    nextProps = {}
    componentWillReceiveProps(nextProps) {
        this.setState({
            message: nextProps.message,
            value: nextProps.message.text
        });
        this.nextProps = nextProps;
    }


    getEditModalContent() {
        return (
            <div className={`edit-message-modal ${(this.nextProps.isShown) ? 'modal-shown' : null}`}  >
                <span>Edit message</span>
                <input className="edit-message-input" type='text' value={this.state.value} onChange={this.onChange}></input>
                <div className="button-flex-container">
                    <button className="edit-message-button" type='button' onClick={this.onSave}>Save</button>
                    <button className="edit-message-close" type='button' onClick={this.onCancel}>Cancel</button>
                </div>
            </div>
        );
    }
    render() {
        const isShown = this.props.isShown;
        return isShown ? this.getEditModalContent() : null;
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        isShown: state.editModal.isShown,
        message: state.editModal.message
    }
}

const mapDispatchToProps = {
    ...actions,
    updateMessage
}
export default connect(mapStateToProps, mapDispatchToProps)(EditModal);