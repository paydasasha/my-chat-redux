import React from 'react'
import Message from '../Message/Message'
import moment from 'moment'
import './MessageList.css'

class MessageList extends React.Component {
    renderMessages = () => {
        const dates = this.props.messages.messages.reduce((acc, message) => {
            const date = moment(message.createdAt).format('DD MMM');
            if (!acc[date]) {
                acc[date] = [];
            }
            acc[date].push(message)
            return acc;
        }, {})

        const days = Object.keys(dates);

        const messages = days.reduce((acc, day) => {
            acc.push({ type: 'separator', day })
            return acc.concat(dates[day])
        }, [])

        return messages.map((message) => {
            if (message.type === 'separator') {
                return <div className='messages-divider' key={message.day}>{message.day}</div>
            }
            return <Message key={message.id} message={message} user={this.props.messages.user} onLike={this.props.onLike} onDelete={this.props.onDelete} onEdit={this.props.onEdit} />
        })


    }
    render() {
        return <div className='message-list'>{this.renderMessages()}</div>

    }
}
export default MessageList;