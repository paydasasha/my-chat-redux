import React from 'react';
import loader from '../../loader.svg';
import './Preloader.css'

class Preloader extends React.Component {
    render() {
        return < img src={loader} className="preloader" alt='' />
    }
}

export default Preloader;