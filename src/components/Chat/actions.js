import { ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE, LIKE_MESSAGE, GET_INITIAL_MESSEGES, TOGGLE_IS_FETCHING } from "./actionTypes";
// import service from './service';

export const addMessage = (newMessage) => ({
    type: ADD_MESSAGE,
    payload: {
        newMessage
    }
});

export const deleteMessage = (id) => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const updateMessage = (message) => ({
    type: UPDATE_MESSAGE,
    payload: {
        message
    }
});

export const likeMessage = (id) => ({
    type: LIKE_MESSAGE,
    payload: {
        id
    }
});

export const getInitialMessages = (res) => ({
    type: GET_INITIAL_MESSEGES,
    payload: {
        res
    }
});

export const toggleIsFetching = (isFetching) => ({
    type: TOGGLE_IS_FETCHING,
    payload: {
        isFetching
    }
})