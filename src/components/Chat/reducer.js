/* eslint-disable array-callback-return */
/* eslint-disable no-fallthrough */
/* eslint-disable default-case */

import { ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE, LIKE_MESSAGE, GET_INITIAL_MESSEGES, TOGGLE_IS_FETCHING } from "./actionTypes";

const user = {
    user: 'Alex',
    userId: 5
};
const message = '';

const initialState = {
    messages: [],
    user: user,
    message: message,
    isFetching: false
};


// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_MESSAGE: {
            const { newMessage } = action.payload;
            return {
                ...state,
                messages: [...state.messages, newMessage],
            }
        }
        case DELETE_MESSAGE: {
            const { id } = action.payload;
            let newMessagesArray = state.messages.filter(item => item.id !== id);
            return {
                ...state,
                messages: newMessagesArray,
            }
        }
        case UPDATE_MESSAGE: {
            const { message } = action.payload;
            let newMessagesArray = state.messages.map(item => {
                if (item.id === message.id) {
                    item = message;
                    return item;
                } else {
                    return item;
                }
            });
            return {
                ...state,
                messages: newMessagesArray,
            }
        }
        case LIKE_MESSAGE: {
            const { id } = action.payload;
            let newMessagesArray = state.messages.map(item => {
                if (item.id === id) {
                    return item = {
                        ...item,
                        isLiked: item.isLiked === true ? false : true
                    }
                } else {
                    return item;
                }
            });
            return {
                ...state,
                messages: newMessagesArray,
            }
        }
        case GET_INITIAL_MESSEGES: {
            const { res } = action.payload;
            return {
                ...state,
                messages: res,
            }
        }
        case TOGGLE_IS_FETCHING: {
            const { isFetching } = action.payload;
            return {
                ...state,
                isFetching: isFetching,
            }
        }
        default:
            return state
    }
}