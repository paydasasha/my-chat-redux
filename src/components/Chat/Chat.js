import React from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import { showModal, setCurrentMessage } from '../EditModal/actions';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import Header from '../Header/Header';
import Preloader from '../Preloader/Preloader';

class Chat extends React.Component {

    componentDidMount() {
        this.props.toggleIsFetching(true);
        fetch(this.props.url).then(res => res.json()).then(res => {
            this.props.getInitialMessages(res);
            this.props.toggleIsFetching(false);
        });
        window.addEventListener('keydown', event => {
            if (event.key === 'ArrowUp') {
                let lastMessage = this.props.messages.messages[this.props.messages.messages.length - 1];
                if (lastMessage.userId === this.props.messages.user.userId) {
                    this.onEditMessage(this.props.messages.messages[this.props.messages.messages.length - 1]);
                }
            };
        });
    }
    onAddMessage = (message) => {
        this.props.addMessage(message);
    }
    onDelete = (message) => {
        this.props.deleteMessage(message.id);
    }
    onLike = (message) => {
        this.props.likeMessage(message.id);
    }
    onEditMessage = (message) => {
        this.props.showModal();
        this.props.setCurrentMessage(message);
    }
    render() {
        return <div>
            {this.props.isFetching ? <Preloader /> : null}
            <Header messages={this.props.messages.messages} />
            <MessageList messages={this.props.messages} onLike={this.onLike} onDelete={this.onDelete} onEdit={this.onEditMessage} />
            <MessageInput user={this.props.messages.user} message={this.props.messages.message} onSubmit={this.onAddMessage} />
        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat,
        isFetching: state.chat.isFetching
    }
};

const mapDispatchToProps = {
    ...actions,
    showModal,
    setCurrentMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);